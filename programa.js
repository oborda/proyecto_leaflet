let mymap = L.map('mapid').setView([5.037824, -73.995983], 16);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

var marker = L.marker([5.037824, -73.995983]).addTo(mymap);
var marker2 = L.marker([5.019206, -74.009515]).addTo(mymap);

L.control.scale().addTo(mymap);
var circle = L.circle([5.037824, -73.995983], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.3,
    radius: 100
}).addTo(mymap);

var popup = L.popup()
    .setLatLng([5.037824, -73.995983])
    .setContent("Esta es mi casa ")
    .openOn(mymap);

var popup2 = L.popup()
    .setLatLng([5.019206, -74.009515])
    .setContent("Esta es la Catedral de Sal")
    .openOn(mymap);

marker.bindPopup("</b>Esta es mi Casa.").openPopup();
marker2.bindPopup("</b>Esta la Catedral de Sal.").openPopup();

var geojsonFeature = {
    "type": "Feature",
    "properties": {
        "name": "Metro",
        "amenity": "Hipermercado",
        "popupContent": "Aqui se puede hacer mercado!"
    },
    "geometry": {
        "type": "Point",
        "coordinates": [-73.992470,5.026216]
    }
};

L.geoJSON(geojsonFeature).addTo(mymap);

var states = [{
    "type": "Feature",
    "properties": {"name": "Estadio Los Zipas"},
    "geometry": {
        "type": "Polygon",
        "coordinates": [[
            [-73.997722,5.026722],
            [-73.997476,5.028336],
            [-73.996164,5.028119],
            [-73.996383,5.026606],
            [-73.996716,5.026435],
            [-73.997722,5.026722]
        ]]
    }
}, {
    "type": "Feature",
    "properties": {"name": "Parque de La Esperanza"},
    "geometry": {
        "type": "Polygon",
        "coordinates": [[
            [-74.00020112, 5.0195774],
            [-74.000051, 5.01964469], 
            [-74.00020862, 5.01989144], 
            [-73.99925538, 5.02090084], 
            [-74.00047133, 5.02208969], 
            [-73.99920284, 5.02338321], 
            [-73.99945804, 5.02352528], 
            [-74.0008241, 5.02197753], 
            [-74.0011994, 5.02217194], 
            [-74.00161222, 5.02162611], 
            [-74.001327, 5.02141676], 
            [-74.00020112, 5.0195774]
        ]]
    }
}];


L.geoJSON(states, {
    style: function(feature) {
        switch (feature.properties.name) {
            case 'Estadio Los Zipas': return {color: "#ff0000"};
            case 'Parque de La Esperanza':   return {color: "#0000ff"};
        }
    }
}).addTo(mymap);

var myGepJSON = {"type": "FeatureCollection", "features": [{"id": "49285f7f-e3c1-4dbc-9765-f4fb8964c0a8", "properties": {"name": "Via"}, "type": "Feature", "geometry": {"type": "LineString", "coordinates": [[-73.9953583, 5.0374649], [-73.99611166, 5.0369546], [-73.99740743, 5.03635424], [-73.99882373, 5.03581392], [-73.99924561, 5.03533363], [-73.99948669, 5.03458319], [-73.99957709, 5.03419295], [-74.00030031, 5.0329322], [-74.00051125, 5.03242189], [-74.00042085, 5.03125119], [-74.00060165, 5.03035065], [-74.00102353, 5.02966023], [-74.0011742, 5.02893979], [-74.00262064, 5.02629819], [-73.99942642, 5.02524754], [-73.99692528, 5.0250074], [-73.99451455, 5.02536762], [-73.99213394, 5.02572784]]}}]}

L.geoJSON(myGepJSON).addTo(mymap)
